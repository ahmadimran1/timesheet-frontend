export class TimeLog{

    public Id:number;
    public Day:string;
    public EmployeeId:number;
    public TaskId:number;
    public Hours:number;
}