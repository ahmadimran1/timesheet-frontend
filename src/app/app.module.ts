import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from './app.component';
import { EmployeeListComponent } from './employee/employee.component';
import { EmployeeService } from './services/employee.service';
import { TimesheetComponent } from './timesheet/timesheet.component';
import { Routes, RouterModule } from '@angular/router';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { FormsModule } from '@angular/forms';
import { TimeLogComponent } from './time-log/time-log.component';
@NgModule({
  declarations: [
    AppComponent,
    EmployeeListComponent,
    TimesheetComponent,
    TimeLogComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    RouterModule,
    AppRoutingModule,
    AngularFontAwesomeModule,
    FormsModule
  ],
  entryComponents: [TimesheetComponent] ,
  providers: [
    EmployeeService
  ],
  bootstrap: [AppComponent],
  exports: [AppRoutingModule]
})
export class AppModule { } 
