import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { RouterModule, Routes } from "@angular/router";
import {EmployeeListComponent} from './employee/employee.component';
import {TimesheetComponent} from './timesheet/timesheet.component';
import {TimeLogComponent} from './time-log/time-log.component';

const routes: Routes = [
  { path: '', component: EmployeeListComponent },
  { path: 'employees', component: EmployeeListComponent },
    { path: 'timesheet/:empId', component: TimesheetComponent },
    { path: 'timelog/:empId', component: TimeLogComponent }
  ];

  @NgModule({
    imports: [CommonModule, RouterModule.forRoot(routes)],
    declarations: [],
    exports: [RouterModule]
  })
  export class AppRoutingModule {


  }