import { TimeLog } from '../Models/TimeLog';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment.prod';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class TimeLogService {
  timeLog: TimeLog[] = [];
  private baseapi = environment.apiUrl;
  constructor(private http: HttpClient) { }

  saveTimeLog(timeLog: TimeLog[]) {
    
      return this.http.post(this.baseapi + "/timesheet/saveTimeLog", timeLog);

  }
}
