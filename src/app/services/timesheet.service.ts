import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment.prod';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { TimeLog } from '../Models/TimeLog';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TimesheetService {
  private baseapi = environment.apiUrl;
  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json'
    })
  };
  constructor(private http: HttpClient) { }

  gettimeSheet(employeeId): Observable<TimeLog[]> {
    return this.http.get<TimeLog[]>(this.baseapi + "/timesheet/getall/" + employeeId);
  
  }
  
}
