import { Component, OnInit } from '@angular/core';
import { EmployeeService } from '../services/employee.service';
import { Router, ActivatedRoute } from '@angular/router';
import { TimeSheet } from '../Models/TimeSheet';
import { TimeLogService } from '../services/time-log.service';
import { TimeLog } from '../Models/TimeLog';
import { TaskService } from '../services/task.service';
import { TimesheetService } from '../services/timesheet.service';

@Component({
  selector: 'app-timesheet',
  templateUrl: './timesheet.component.html',
  styleUrls: ['./timesheet.component.scss']
})
export class TimesheetComponent implements OnInit {
  employees: any;
  logs: any[];
  employeeId: number;
  timeSheet: TimeSheet[];
  tasks: any;
  constructor(private timesheetService: TimesheetService, private taskService: TaskService, private employeeService: EmployeeService, private router: Router, private route: ActivatedRoute, private timeLogService: TimeLogService) {
    this.employeeId = this.route.snapshot.params["empId"];

    this.employeeService.getallemployees().subscribe(data => {
      this.employees = data;
    });



    this.taskService.getAllTasks().subscribe(data => {
      this.tasks = data;
      this.LoadTimeSheetData();

    });
  }
  LoadTimeSheetData() {

    this.timesheetService.gettimeSheet(this.employeeId).subscribe(data => {
      let data1 = this.timeLogService.timeLog.filter(x => x.Id == -1);
      this.timeLogService.timeLog = data;
      data1.forEach(x => this.timeLogService.timeLog.push(x));
      this.mapper();
    });

  }

  ngOnInit() {
  }


  mapper() {
    this.timeSheet = [];
    this.timeLogService.timeLog.filter(x => x.EmployeeId == this.employeeId).forEach(timeLog => {
      var data = this.timeSheet.find(x => x.TaskId == timeLog.TaskId);
      if (timeLog.Day == 'Sunday') {
        if (data != null) {
          data.Sunday += timeLog.Hours;
        }
        else {
          var newObj = this.makeNewTimeSheetObj(timeLog);
          newObj.Sunday += timeLog.Hours;
          this.timeSheet.push(newObj);
        }
      }
      else if (timeLog.Day == 'Monday') {
        if (data != null) {
          data.Monday += timeLog.Hours;
        }
        else {
          var newObj = this.makeNewTimeSheetObj(timeLog);
          newObj.Monday += timeLog.Hours;
          this.timeSheet.push(newObj);
        }
      }
      else if (timeLog.Day == 'Tuesday') {
        if (data != null) {
          data.Tuesday += timeLog.Hours;
        }
        else {
          var newObj = this.makeNewTimeSheetObj(timeLog);
          newObj.Tuesday += timeLog.Hours;
          this.timeSheet.push(newObj);
        }
      }
      else if (timeLog.Day == 'Wednesday') {
        if (data != null) {
          data.Wednesday += timeLog.Hours;
        }
        else {
          var newObj = this.makeNewTimeSheetObj(timeLog);
          newObj.Wednesday += timeLog.Hours;
          this.timeSheet.push(newObj);
        }
      }
      else if (timeLog.Day == 'Thursday') {
        if (data != null) {
          data.Thursday += timeLog.Hours;
        }
        else {
          var newObj = this.makeNewTimeSheetObj(timeLog);
          newObj.Thursday += timeLog.Hours;
          this.timeSheet.push(newObj);
        }
      }
      else if (timeLog.Day == 'Friday') {
        if (data != null) {
          data.Friday += timeLog.Hours;
        }
        else {
          var newObj = this.makeNewTimeSheetObj(timeLog);
          newObj.Friday += timeLog.Hours;
          this.timeSheet.push(newObj);
        }
      }
      else if (timeLog.Day == 'Saturday') {
        if (data != null) {
          data.Saturday += timeLog.Hours;
        }
        else {
          var newObj = this.makeNewTimeSheetObj(timeLog);
          newObj.Saturday += timeLog.Hours;
          this.timeSheet.push(newObj);
        }
      }
    });
    console.log(this.timeSheet)
  }
  makeNewTimeSheetObj(obj: TimeLog) {
    var objTimeSheet: TimeSheet = new TimeSheet();
    objTimeSheet.TaskId = obj.TaskId;
    objTimeSheet.TaskName = this.tasks.find(x => x.Id == obj.TaskId).Name;
    return objTimeSheet;
  }
  ChangeEmployee() {
    this.LoadTimeSheetData();
  }
  addLog() {
    this.router.navigate(['/timelog', this.employeeId]);
  }
  Save() {
    let dataForSave = this.timeLogService.timeLog.filter(x => x.EmployeeId == this.employeeId && x.Id == -1);
    if (dataForSave.length > 0) {
      this.timeLogService.saveTimeLog(dataForSave).subscribe(data => {
        this.router.navigate(['/employees']);

      });
    }
    else {
      this.router.navigate(['/employees']);
    }
  }
  Cancel() {
    this.router.navigate(['/employees']);
  }

}
