import { Component, OnInit } from '@angular/core';
import { TimeLog } from '../Models/TimeLog';
import { TimesheetService } from '../services/timesheet.service';
import { EmployeeService } from '../services/employee.service';
import { TaskService } from '../services/task.service';
import { Router, ActivatedRoute } from '@angular/router';
import { TimeLogService } from '../services/time-log.service';

@Component({
  selector: 'app-time-log',
  templateUrl: './time-log.component.html',
  styleUrls: ['./time-log.component.scss']
})
export class TimeLogComponent implements OnInit {
  timeLog: TimeLog;

  employees: any;
  tasks: any;
  employeeId: number;
  Days:string[]=["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"];
  constructor(private route: ActivatedRoute,private timesheetService: TimesheetService, private employeeService: EmployeeService, private taskService: TaskService,private router: Router,private timeLogService:TimeLogService) {
    this.timeLog=new TimeLog();
    this.timeLog.EmployeeId = this.route.snapshot.params["empId"];
    
    this.employeeService.getallemployees().subscribe(data => {
      this.employees = data;
    });
    this.taskService.getAllTasks().subscribe(data => {
      this.tasks = data;
    });
  }

  ngOnInit() {
  }
  //Save log time 
  Save() {
    this.timeLog.Id=-1;
    this.timeLogService.timeLog.push(this.timeLog);
    this.router.navigate(['/timesheet',this.timeLog.EmployeeId]);
  }
  Cancel(){
    this.router.navigate(['/timesheet',this.timeLog.EmployeeId]);
  }
}
